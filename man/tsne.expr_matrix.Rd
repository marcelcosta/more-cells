\name{tsne.expr_matrix}
\alias{tsne.expr_matrix}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Function to generate an expression matrix from a GatingSet.
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
This function takes a GatingSet from the package "openCyto" and generates a matrix containing concatenated cells from different samples. You can specify the maximum number of cells per sample and the node of the GatingSet to extract. 
}
\usage{
tsne.expr_matrix(par, par_ind, population, gs_int=gs, cutoff=1000, seed=123)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{par}{
%%     ~~Describe \code{par} here~~
Labels the parameters you want to include inthe expression matrix. You can specify which molecules correspond to each fluorescence channel. You don't need to include all the channels that were adquired by the cytometer as long as you include the correct indexes in the "par_ind" argument.
}
  \item{par_ind}{
%%     ~~Describe \code{par_ind} here~~
Index (position) in the column order for the parameters included in the "par" argument.
}
  \item{population}{
%%     ~~Describe \code{population} here~~
Node from the Gatingset from which you want to extract the cell expression. For example: NK cells.
}
  \item{gs_int}{
%%     ~~Describe \code{gs_int} here~~
Specifies the GatingSet object. Default "gs".
}
  \item{cutoff}{
%%     ~~Describe \code{cutoff} here~~
Specifies the maximum number of cells per sample to randomly extract. Default "1000".
}
  \item{seed}{
%%     ~~Describe \code{cutoff} here~~
You can specify a seed for reproducibility (i.e. with the same samples and same seed, you will always pick the same cells). Default "123".
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
The function iterates and randomly selects a number of cells from each sample of a GatingSet. You can specify the molecules each channel refers to (e.g. CD4). In this case, you must specify its position in the parameter vector. It can be accessed by executing "colnames(gs)".
The output is a list with two elements. The first contains the expression matrix itself. The second is a vector representing the position each cell has in the expression matrix and its value refers to the sample that the cell was selected from. You can use this vector to represent data by sample.
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
A list with the following components:
\item{comp1 }{A matrix including the expression values of cells concatenated from several samples in a GatingSet}
\item{comp2 }{A vector where each value is the sample each cell came from.}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
Marcel Costa
Michelle Ataya
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

ws<-openWorkspace("Files/20180320.wsp") #Here you have to specify the path to your WorkSpace FlowJo file.
gs<-parseWorkspace(ws, name= "All Samples", isNcdf = TRUE)

markers<-c("HLA-DR", "CD3", "CD56","NKG2C")
markers_index<-c(4,5,9,12)
cut<-2000
pop<-"NK cells"

list_results<-tsne.expr_matrix(par=markers, par_ind=markers_index, population=pop, cutoff=cut)
data_total<-list_results[1][[1]]
donors<-list_results[2][[1]]

## The function is currently defined as
tsne.expr_matrix<-function(par, par_ind, population, gs_int=gs, cutoff=1000, seed=123){
    npar<-length(par)
    data_total<-matrix(nrow = 0, ncol=npar)
    donor<-vector()
    for (num in 1:length(sampleNames(gs_int))){
      print(num)
      print(sampleNames(gs_int[[num]]))
      data <- flowCore::exprs(getData(gs_int[[num]], population))
      colnames(data)[par_ind]<-par
      colnames_proj <- unname(colnames(data))[par_ind]
      
      if (nrow(data) < cutoff) {
        nsub <- nrow(data)
      }else{
        nsub <- cutoff
      }
      if (!is.null(seed)){
          set.seed(123)
      }
      data <- data[sample(1:nrow(data), nsub), ]
      data <- data[, colnames_proj]
      data <- data[!duplicated(data),]
      data_total<-rbind(data_total, data)
      donor<-append(donor, rep(sampleNames(gs_int)[num], length(data[,1])))
    }
    return(list(data_total, donor))
}
}
