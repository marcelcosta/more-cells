\name{morecells-package}
\alias{morecells-package}
\alias{morecells}
\docType{package}
\title{
\packageTitle{morecells}
}
\description{
\packageDescription{morecells}
}
\details{
Requires a GatingSet from openCyto package and uses the RtSNE and Rphenograph packages for multidimensional analysis.

The DESCRIPTION file:
\packageDESCRIPTION{morecells}
\packageIndices{morecells}
%~~ An overview of how to use the package, including the most important ~~
%~~ functions ~~
}
\author{
\packageAuthor{morecells}

Maintainer: \packageMaintainer{morecells}
}
%\references{
%~~ Literature or other references for background information ~~
%}
%~~ Optionally other standard keywords, one per line, from file KEYWORDS in ~~
%~~ the R documentation directory ~~
\keyword{ package }
%\seealso{
%~~ Optional links to other man pages, e.g. ~~
%~~ \code{\link[<pkg>:<pkg>-package]{<pkg>}} ~~
%}
%%\examples{
%%~~ simple examples of the most important functions ~~
%%}
