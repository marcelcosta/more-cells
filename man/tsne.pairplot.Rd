\name{tsne.pairplot}
\alias{tsne.pairplot}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Function to plot the result of pairplot wilcoxon test from a pairplot matrix.
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
tsne.pairplot(matrix, guides = T, title = F, text = F)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{matrix}{
%%     ~~Describe \code{matrix} here~~
}
  \item{guides}{
%%     ~~Describe \code{guides} here~~
}
  \item{title}{
%%     ~~Describe \code{title} here~~
}
  \item{text}{
%%     ~~Describe \code{text} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (matrix, guides = T, title = F, text = F) 
{
    edge_test <- as.data.frame(matrix(ncol = 3, nrow = 0))
    for (col in 1:ncol(matrix)) {
        for (row in 1:nrow(matrix)) {
            edge_test <- rbind(edge_test, as.data.frame(matrix(c(colnames(matrix)[col], 
                rownames(matrix)[row], matrix[row, col]), nrow = 1)))
        }
    }
    edge_test$V1 <- as.factor(edge_test$V1)
    edge_test$V2 <- as.factor(edge_test$V2)
    edge_test <- edge_test[!is.na(edge_test$V3), ]
    edge_test$V3 <- as.numeric(as.character(edge_test$V3))
    edge_test$V4 <- factor(0, levels = c(0, 1))
    edge_test$V4[edge_test$V3 < 0.05] <- as.factor(0)
    edge_test$V4[edge_test$V3 >= 0.05] <- as.factor(1)
    edge_test$V5 <- edge_test$V3
    edge_test$V5[edge_test$V3 < 0.05 & edge_test$V3 >= 0.01] <- "*"
    edge_test$V5[edge_test$V3 < 0.01 & edge_test$V3 >= 0.001] <- "**"
    edge_test$V5[edge_test$V3 < 0.001] <- "***"
    edge_test$V5[edge_test$V3 >= 0.05] <- ""
    g <- ggplot(edge_test, aes(V1, V2)) + geom_tile(aes(fill = as.factor(V4)), 
        color = "black") + scale_fill_manual(values = c("black", 
        "white"), labels = c("p < 0.05", "p >= 0.05"), drop = FALSE) + 
        labs(fill = "Significance", x = "", y = "") + scale_x_discrete(limits = unique(edge_test$V1)) + 
        scale_y_discrete(limits = rev(unique(edge_test$V2))) + 
        theme_minimal() + theme(axis.text.x = element_text(angle = 45, 
        hjust = 1), aspect.ratio = 1)
    if (guides == F) {
        g <- g + guides(fill = F)
    }
    if (title != F) {
        g <- g + ggtitle(title)
    }
    if (text == "text") {
        g <- g + geom_text(aes(label = round(edge_test$V3, 3), 
            colour = as.factor(edge_test$V4)), size = 2) + scale_colour_manual(values = c("yellow", 
            "black")) + guides(color = F)
    }
    if (text == "text") {
        g <- g + geom_text(aes(label = round(edge_test$V3, 3), 
            colour = as.factor(edge_test$V4)), size = 2) + scale_colour_manual(values = c("yellow", 
            "black"), drop = F) + guides(color = F)
    }
    if (text == "symbol") {
        g <- g + geom_text(aes(label = V5), colour = "yellow", 
            size = 4)
    }
    return(g)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
